from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.operators.docker_operator import DockerOperator

default_args = {
        'owner'                 : 'airflow',
        'description'           : 'Running Retina Batch Jobs',
        'depend_on_past'        : False,
        'start_date'            : datetime(2021, 3, 15),
        'email_on_failure'      : False,
        'email_on_retry'        : False,
        'retries'               : 1,
        'retry_delay'           : timedelta(minutes=2)
}

dag_config =  Variable.get("kpidatainput_variable", deserialize_json=True)
fileName = dag_config["fileName"]
clientId = dag_config["clientId"]

with DAG('retinakpidatainputbatchdag', default_args=default_args, schedule_interval="* * * * *", catchup=False) as dag:

        t1 = BashOperator(
                task_id="get_kpi_variables",
                bash_command='echo "{0}"'.format(dag_config),
        )
        
        t2 = BashOperator(
                task_id='First_Task_Print_Current_Date',
                bash_command='date'
        )
        
        t3 = DockerOperator(
                task_id='First_Task_Run_KpiDataInput_Batch_Job',
                image='retina-kpidatainput-batch:0.0.1-SNAPSHOT',
                api_version='auto',
                auto_remove=True,
                command="/bin/sleep 30",
                docker_url="unix://var/run/docker.sock",
                mem_limit='600mb',
                network_mode="bridge"
        )

        t4 = BashOperator(
                task_id='Third_Task_Print_Success_Message',
                bash_command='echo "RetinaBatch execution done."'
        )

        t1 >> t2 >> t3 >> t4
