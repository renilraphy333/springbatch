CREATE TABLE IF NOT EXISTS retina.packing_productivity (
	`packing_productivity_id` INT(11) NOT NULL AUTO_INCREMENT,
	`tenant_id` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`feature_id` long NOT NULL COLLATE 'latin1_swedish_ci',
	`week` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`value` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`date` DATE NULL DEFAULT NULL,
	`unit` VARCHAR(10) NOT NULL COLLATE 'latin1_swedish_ci',
	`create_by` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`create_timestamp` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	`update_by` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`update_timestamp` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`packing_productivity_id`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=20150
;
