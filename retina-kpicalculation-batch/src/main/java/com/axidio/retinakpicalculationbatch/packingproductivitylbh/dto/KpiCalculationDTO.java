package com.axidio.retinakpicalculationbatch.packingproductivitylbh.dto;

public class KpiCalculationDTO {

    private String featureId;
    private String clientId;
    private String fileName;
    private String date;

    public KpiCalculationDTO() {
    this.featureId = "";
    this.clientId = "";
    this.fileName = "";
    this.date = "";
    }

    public KpiCalculationDTO(String featureId,String clientId,String fileName,String date) {
        this.featureId = featureId;
        this.clientId = clientId;
        this.fileName = fileName;
        this.date = date;
    }

    public String getFeatureId() {
        return featureId;
    }
    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }
    public String getClientId() {
        return clientId;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    @Override
    public String toString() {
        return "["+featureId+","+clientId+","+fileName+","+date+"]";
    }    
    
}
