package com.axidio.retinakpicalculationbatch.packingproductivitylbh.job;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.support.ListPreparedStatementSetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.PreparedStatementSetter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.axidio.retinakpicalculationbatch.packingproductivitylbh.listener.JobCompleteNotificationListener;
import com.axidio.retinakpicalculationbatch.packingproductivitylbh.mapper.KpiCalculationDbRowMapper;
import com.axidio.retinakpicalculationbatch.packingproductivitylbh.model.PackingProductivity;
import com.axidio.retinakpicalculationbatch.packingproductivitylbh.utils.Constants;


@Configuration
@EnableBatchProcessing
public class KpiCaluclationJob {

    private JobBuilderFactory jobBuilderFactory;
    private StepBuilderFactory stepBuilderFactory;
    private DataSource dataSource;
    private String clientId = null;
    private Long featureId = 0L;

    @Autowired
    public KpiCaluclationJob(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,  DataSource dataSource){
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.dataSource = dataSource;
    }

    @Qualifier(value = "kpiCalculateJob")
    @Bean
    public Job packingProductivityJobBuilder() throws Exception {
        return this.jobBuilderFactory.get("kpicalculationjob")
                .start(step1KpiCalculation())
                .listener(jobCompleteNotificationListener())
                .build();
    }


    @Bean
    public Step step1KpiCalculation() throws Exception{
        return this.stepBuilderFactory.get("step1")
                .<PackingProductivity,PackingProductivity>chunk(10)
                .reader(workOrderDBReader())
                .writer(packingProductivityDBWriterDefault())
                .build();
    }

    void getClientIdValue(@Value("#{jobParameters[clientId]}") final String clientId) throws Exception {
        System.out.println("---------------------------------"+clientId);
        this.clientId = clientId;
    }


    void getFeatureIdValue(@Value("#{jobParameters[featureId]}") final Long featureId) throws Exception {
        this.featureId = featureId;
    }

    @Bean
    @StepScope
    public ItemStreamReader<PackingProductivity> workOrderDBReader() throws Exception{
        List<String> parameters = new ArrayList<>();
        JdbcCursorItemReader<PackingProductivity> jdbcCursorItemReader = new JdbcCursorItemReader<>();
        jdbcCursorItemReader.setDataSource(dataSource);
        jdbcCursorItemReader.setSql("SELECT a.tenant_id,truncate(AVG(a.total_pieces/a.total_time),2) AS 'value',a.date as 'week' FROM retina.work_orders a WHERE a.job_id = 'Packng' and a.tenant_id = ? GROUP BY a.date ORDER BY a.date DESC LIMIT 5");
        parameters.add(Constants.client_Value);
        ListPreparedStatementSetter pss = new ListPreparedStatementSetter(parameters);
        jdbcCursorItemReader.setPreparedStatementSetter(pss);
        jdbcCursorItemReader.setRowMapper(new KpiCalculationDbRowMapper());

        return jdbcCursorItemReader;

    }


    @Bean
    public JdbcBatchItemWriter<PackingProductivity> packingProductivityDBWriterDefault() {
        JdbcBatchItemWriter<PackingProductivity> itemWriter1 = new JdbcBatchItemWriter<PackingProductivity>();
        itemWriter1.setDataSource(dataSource);
        itemWriter1.setSql("insert into packing_productivity (tenant_id,feature_id,week,value,date,unit) values (:clientId, :featureId, :week, :value, :date, :unit)");
        itemWriter1.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<PackingProductivity>());
        return itemWriter1;
    }

    @Bean
    public JobCompleteNotificationListener jobCompleteNotificationListener(){
        return new JobCompleteNotificationListener();
    }

}