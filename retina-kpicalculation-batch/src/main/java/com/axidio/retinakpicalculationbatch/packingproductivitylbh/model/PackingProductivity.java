package com.axidio.retinakpicalculationbatch.packingproductivitylbh.model;

import java.util.Date;

public class PackingProductivity {

    private int packingProductivityId;
    private String clientId;
    private String featureId;
    private String week;
    private String value;
    private Date date;
    private String unit;

    public PackingProductivity(){
        this.packingProductivityId = 0;
        this.clientId = "";
        this.featureId = "";
        this.week = "";
        this.value = "";
        this.unit = "";
    }
    
    public PackingProductivity(int packingProductivityId,String clientId,String featureId,String week,String value,Date date,String unit){

        this.packingProductivityId = packingProductivityId;
        this.clientId = clientId;
        this.featureId = featureId;
        this.week = week;
        this.value = value;
        this.date = date;
        this.unit = unit;

    }
    
    /**
     * @return the packingProductivityId
     */
    public int getPackingProductivityId() {
        return packingProductivityId;
    }
    /**
     * @param packingProductivityId the packingProductivityId to set
     */
    public void setPackingProductivityId(int packingProductivityId) {
        this.packingProductivityId = packingProductivityId;
    }
    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }
    /**
     * @param clientId the clientId to set
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    /**
     * @return the featureId
     */
    public String getFeatureId() {
        return featureId;
    }
    /**
     * @param featureId the featureId to set
     */
    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }
    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }
    /**
     * @return the week
     */
    public String getWeek() {
        return week;
    }
    /**
     * @param week the week to set
     */
    public void setWeek(String week) {
        this.week = week;
    }
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }
    /**
     * @param unit the unit to set
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }
    @Override
    public String toString() {
        return "["+packingProductivityId+","+clientId+","+featureId+","+week+","+value+","+date+","+unit+"]";
    }
}