package com.axidio.retinakpicalculationbatch.packingproductivitylbh.controller;

import java.util.Date;

import com.axidio.retinakpicalculationbatch.packingproductivitylbh.dto.KpiCalculationDTO;
import com.axidio.retinakpicalculationbatch.packingproductivitylbh.utils.Constants;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
url: http://localhost:8082/runbatch/packingproductivityjob
 */

@RestController
@RequestMapping("/runbatch")
public class JobController {


    @Autowired
    private JobLauncher jobLauncher;

    
    @Autowired
    private Job kpiCalculateJob;


    @RequestMapping(value = "/packingproductivityjob")
    public String runKpiCalculationJob(@RequestBody KpiCalculationDTO kpiCalculationDTO) {

        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("clientId", kpiCalculationDTO.getClientId());
        Constants.client_Value = kpiCalculationDTO.getClientId();
        jobParametersBuilder.addString("featureId", kpiCalculationDTO.getFeatureId());
        Constants.feature_Value = kpiCalculationDTO.getFeatureId();
        jobParametersBuilder.addDate("date", new Date(), true);
        try {
            jobLauncher.run(kpiCalculateJob, jobParametersBuilder.toJobParameters());
        } catch (Exception e) {
            e.printStackTrace();
        } 

        return String.format("Packing Productivity job submitted successfully.");
    }
    

}