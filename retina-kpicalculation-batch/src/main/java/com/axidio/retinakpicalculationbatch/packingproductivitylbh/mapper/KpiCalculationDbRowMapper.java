package com.axidio.retinakpicalculationbatch.packingproductivitylbh.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.axidio.retinakpicalculationbatch.packingproductivitylbh.model.PackingProductivity;
import com.axidio.retinakpicalculationbatch.packingproductivitylbh.utils.Constants;

import org.springframework.jdbc.core.RowMapper;

public class KpiCalculationDbRowMapper implements RowMapper<PackingProductivity> {



    @Override
    public PackingProductivity mapRow(ResultSet resultSet, int i) throws SQLException {
        
        Date date1;
        PackingProductivity packingProductivity = new PackingProductivity();
        try {
            date1 = new SimpleDateFormat("yyyy-mm-dd").parse(resultSet.getString("week"));
            packingProductivity.setClientId(resultSet.getString("tenant_id"));
            // packingProductivity.setFeatureId(this.featureId);
            packingProductivity.setFeatureId(Constants.feature_Value);
            packingProductivity.setWeek(resultSet.getString("week"));
            packingProductivity.setValue(resultSet.getString("value"));
            packingProductivity.setDate(date1);
            packingProductivity.setUnit("Lines");
            System.out.println(packingProductivity);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        return packingProductivity;

    }

}