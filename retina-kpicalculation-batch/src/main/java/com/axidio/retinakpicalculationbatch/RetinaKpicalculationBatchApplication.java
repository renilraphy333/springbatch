package com.axidio.retinakpicalculationbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetinaKpicalculationBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetinaKpicalculationBatchApplication.class, args);
	}

}
