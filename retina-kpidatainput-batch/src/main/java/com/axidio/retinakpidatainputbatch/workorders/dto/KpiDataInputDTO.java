package com.axidio.retinakpidatainputbatch.workorders.dto;

public class KpiDataInputDTO {

    private Long featureId;
    private String clientId;
    private String fileName;
    private String date;

    public KpiDataInputDTO() {
    this.featureId = 0L;
    this.clientId = "";
    this.fileName = "";
    this.date = "";
    }

    public KpiDataInputDTO(Long featureId,String clientId,String fileName,String date) {
        this.featureId = featureId;
        this.clientId = clientId;
        this.fileName = fileName;
        this.date = date;
    }

    public Long getFeatureId() {
        return featureId;
    }
    public void setFeatureId(Long featureId) {
        this.featureId = featureId;
    }
    public String getClientId() {
        return clientId;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    @Override
    public String toString() {
        return "["+featureId+","+clientId+","+fileName+","+date+"]";
    }    
    
}
