package com.axidio.retinakpidatainputbatch.workorders.mapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.axidio.retinakpidatainputbatch.workorders.dto.WorkOrdersDTO;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

public class KpiDataInputFileRowMapper implements FieldSetMapper<WorkOrdersDTO> {

    @Override
    public WorkOrdersDTO mapFieldSet(FieldSet fieldSet) {
        WorkOrdersDTO workOrders = new WorkOrdersDTO();
        try {

            DateFormat formatter = new SimpleDateFormat("mm/dd/yyyy"); 
            Date date = (Date)formatter.parse(fieldSet.readString("date"));
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-mm-dd");
            
            workOrders.setWorkOrdersId(fieldSet.readInt("workOrdersId"));
            workOrders.setDcId(fieldSet.readString("dcId"));
            workOrders.setEmployeeId(fieldSet.readString("employeeId"));
            workOrders.setType(fieldSet.readString("type"));
            workOrders.setShift(fieldSet.readString("shift"));
            workOrders.setJobId(fieldSet.readString("jobId"));
            workOrders.setDate(newFormat.format(date));
            workOrders.setAssignTime(fieldSet.readString("assignTime"));
            workOrders.setEndTime(fieldSet.readString("endTime"));
            workOrders.setTotalTime(fieldSet.readInt("totalTime"));
            workOrders.setEquipment(fieldSet.readString("equipment"));
            workOrders.setEquipmentName(fieldSet.readString("equipmentName"));
            workOrders.setEquipmentUsageHours(fieldSet.readString("equipmentUsageHours"));
            workOrders.setTotalPieces(fieldSet.readInt("totalPieces"));
            workOrders.setTotalPallets(fieldSet.readInt("totalPallets"));
            workOrders.setTotalCases(fieldSet.readInt("totalCases"));
            workOrders.setTotalLines(fieldSet.readInt("totalLines"));
            workOrders.setTotalAccuracy(fieldSet.readString("totalAccuracy"));
            workOrders.setTenantId(fieldSet.readString("tenantId"));
            
        } catch (Exception e) {

        }
        return workOrders;
    }

}