package com.axidio.retinakpidatainputbatch.workorders.dto;


public class WorkOrdersDTO {

    private int workOrdersId;
    private String dcId;
    private String employeeId;
    private String type;
    private String shift;
    private String jobId;
    private String date;
    private String assignTime;
    private String endTime;
    private int totalTime;
    private String equipment;
    private String equipmentName;
    private String equipmentUsageHours;
    private int totalPieces;
    private int totalPallets;
    private int totalCases;
    private int totalLines;
    private String totalAccuracy;
    private String tenantId;
    
    public WorkOrdersDTO(){
        
        this.workOrdersId = 0;
        this.dcId = "";
        this.employeeId = "";
        this.type = "";
        this.shift = "";
        this.jobId = "";
        this.date = "";
        this.assignTime = "";
        this.endTime = "";
        this.totalTime = 0;
        this.equipment = "";
        this.equipmentName = "";
        this.equipmentUsageHours = "";
        this.totalPieces = 0;
        this.totalPallets = 0;
        this.totalCases = 0;
        this.totalLines = 0;
        this.totalAccuracy = "";
        this.tenantId = "";

    }
    public WorkOrdersDTO(int workOrdersId,String dcId,String employeeId,String type,String shift,String jobId,String date,String assignTime,String endTime,int totalTime,
    String equipment,String equipmentName,String equipmentUsageHours,int totalPieces,int totalPallets,int totalCases,int totalLines,String totalAccuracy,String tenantId){
        this.workOrdersId = workOrdersId;
        this.dcId = dcId;
        this.employeeId = employeeId;
        this.type = type;
        this.shift = shift;
        this.jobId = jobId;
        this.date = date;
        this.assignTime = assignTime;
        this.endTime = endTime;
        this.totalTime = totalTime;
        this.equipment = equipment;
        this.equipmentName = equipmentName;
        this.equipmentUsageHours = equipmentUsageHours;
        this.totalPieces = totalPieces;
        this.totalPallets = totalPallets;
        this.totalCases = totalCases;
        this.totalLines = totalLines;
        this.totalAccuracy = totalAccuracy;
        this.tenantId = tenantId;

    } 

    public int getWorkOrdersId() {
        return workOrdersId;
    }
    public void setWorkOrdersId(int workOrdersId) {
        this.workOrdersId = workOrdersId;
    }
    public String getDcId() {
        return dcId;
    }
    public void setDcId(String dcId) {
        this.dcId = dcId;
    }
    public String getEmployeeId() {
        return employeeId;
    }
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getShift() {
        return shift;
    }
    public void setShift(String shift) {
        this.shift = shift;
    }
    public String getJobId() {
        return jobId;
    }
    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getAssignTime() {
        return assignTime;
    }
    public void setAssignTime(String assignTime) {
        this.assignTime = assignTime;
    }
    public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    public int getTotalTime() {
        return totalTime;
    }
    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }
    public String getEquipment() {
        return equipment;
    }
    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }
    public String getEquipmentName() {
        return equipmentName;
    }
    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }
    public String getEquipmentUsageHours() {
        return equipmentUsageHours;
    }
    public void setEquipmentUsageHours(String equipmentUsageHours) {
        this.equipmentUsageHours = equipmentUsageHours;
    }
    public int getTotalPieces() {
        return totalPieces;
    }
    public void setTotalPieces(int totalPieces) {
        this.totalPieces = totalPieces;
    }
    public int getTotalPallets() {
        return totalPallets;
    }
    public void setTotalPallets(int totalPallets) {
        this.totalPallets = totalPallets;
    }
    public int getTotalCases() {
        return totalCases;
    }
    public void setTotalCases(int totalCases) {
        this.totalCases = totalCases;
    }
    public int getTotalLines() {
        return totalLines;
    }
    public void setTotalLines(int totalLines) {
        this.totalLines = totalLines;
    }
    public String getTotalAccuracy() {
        return totalAccuracy;
    }
    public void setTotalAccuracy(String totalAccuracy) {
        this.totalAccuracy = totalAccuracy;
    }
    public String getTenantId() {
        return tenantId;
    }
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }
    @Override
    public String toString() {
        return super.toString();
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }


    

}