package com.axidio.retinakpidatainputbatch.workorders.processor;


import com.axidio.retinakpidatainputbatch.workorders.dto.WorkOrdersDTO;
import com.axidio.retinakpidatainputbatch.workorders.model.WorkOrders;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class KpiDataInputProcessor implements ItemProcessor<WorkOrdersDTO, WorkOrders> {

    @Override
    public WorkOrders process(WorkOrdersDTO workOrdersDTOReceived) throws Exception {
        WorkOrders workOrders = new WorkOrders();

        WorkOrdersDTO workOrdersDTO = isValidValues(workOrdersDTOReceived);

        workOrders.setDcId(workOrdersDTO.getDcId());
        workOrders.setEmployeeId(workOrdersDTO.getEmployeeId());
        workOrders.setType(workOrdersDTO.getType());
        workOrders.setShift(workOrdersDTO.getShift());
        workOrders.setJobId(workOrdersDTO.getJobId());
        workOrders.setDate(workOrdersDTO.getDate());
        workOrders.setAssignTime(workOrdersDTO.getAssignTime());
        workOrders.setEndTime(workOrdersDTO.getEndTime());
        workOrders.setTotalTime(workOrdersDTO.getTotalTime());
        workOrders.setEquipment(workOrdersDTO.getEquipment());
        workOrders.setEquipmentName(workOrdersDTO.getEquipmentName());
        workOrders.setEquipmentUsageHours(workOrdersDTO.getEquipmentUsageHours());
        workOrders.setTotalPieces(workOrdersDTO.getTotalPieces());
        workOrders.setTotalPallets(workOrdersDTO.getTotalPallets());
        workOrders.setTotalCases(workOrdersDTO.getTotalCases());
        workOrders.setTotalLines(workOrdersDTO.getTotalLines());
        workOrders.setTotalAccuracy(workOrdersDTO.getTotalAccuracy());
        workOrders.setTenantId(workOrdersDTO.getTenantId());

        System.out.println("inside processor " + workOrders.toString());
        return workOrders;
    }

    public WorkOrdersDTO isValidValues(WorkOrdersDTO workOrdersDTO){

        if(workOrdersDTO.getEquipment().equals("")){
            workOrdersDTO.setEquipment("null");
        }
        if(workOrdersDTO.getTotalAccuracy().equals("")){
            workOrdersDTO.setTotalAccuracy("null");
        }
        return workOrdersDTO;
    }
}