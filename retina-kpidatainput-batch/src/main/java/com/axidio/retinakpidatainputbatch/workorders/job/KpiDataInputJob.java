package com.axidio.retinakpidatainputbatch.workorders.job;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;

import com.axidio.retinakpidatainputbatch.workorders.dto.WorkOrdersDTO;
import com.axidio.retinakpidatainputbatch.workorders.listener.JobCompleteNotificationListener;
import com.axidio.retinakpidatainputbatch.workorders.mapper.KpiDataInputFileRowMapper;
import com.axidio.retinakpidatainputbatch.workorders.model.WorkOrders;
import com.axidio.retinakpidatainputbatch.workorders.processor.KpiDataInputProcessor;

@Configuration
@EnableBatchProcessing
public class KpiDataInputJob {

    private JobBuilderFactory jobBuilderFactory;
    private StepBuilderFactory stepBuilderFactory;
    private KpiDataInputProcessor kpiDataInputProcessor;
    private DataSource dataSource;

    @Autowired
    public KpiDataInputJob(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory, KpiDataInputProcessor kpiDataInputProcessor, DataSource dataSource){
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.kpiDataInputProcessor = kpiDataInputProcessor;
        this.dataSource = dataSource;
    }

    @Qualifier(value = "kpiDataInputJob")
    @Bean
    public Job dataInputJob() throws Exception {
        return this.jobBuilderFactory.get("kpidatainputjob")
                .start(step1KpiDataInput())
                .listener(jobCompleteNotificationListener())
                .build();
    }

    @Bean
    public Step step1KpiDataInput() throws Exception {
        return this.stepBuilderFactory.get("step1")
                .<WorkOrdersDTO, WorkOrders>chunk(5)
                .reader(workOrdersReader())
                .processor(kpiDataInputProcessor)
                .writer(workOrdersDBWriterDefault())
                .build();
    }

    @Bean
    @StepScope
    Resource inputFileResource(@Value("#{jobParameters[fileName]}") final String fileName) throws Exception {
        return new ClassPathResource(fileName);
    }

    @Bean
    @StepScope
    public FlatFileItemReader<WorkOrdersDTO> workOrdersReader() throws Exception {
        FlatFileItemReader<WorkOrdersDTO> reader = new FlatFileItemReader<>();
        reader.setResource(inputFileResource(null));
        reader.setLinesToSkip(1);
        reader.setLineMapper(new DefaultLineMapper<WorkOrdersDTO>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames("workOrdersId", "dcId", "employeeId", "type", "shift",
                        "jobId", "date", "assignTime", "endTime", "totalTime",
                        "equipment", "equipmentName", "equipmentUsageHours", "totalPieces", "totalPallets",
                        "totalCases", "totalLines", "totalAccuracy", "tenantId");
                setDelimiter(",");
            }});
            setFieldSetMapper(new KpiDataInputFileRowMapper());
        }});
        return reader;
    }

    @Bean
    public JdbcBatchItemWriter<WorkOrders> workOrdersDBWriterDefault() {
        
        JdbcBatchItemWriter<WorkOrders> itemWriter = new JdbcBatchItemWriter<WorkOrders>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql("insert into work_orders (dc_id,employee_id,type,shift,job_id,date,assign_time,end_time,total_time,equipment,equipment_name,equipment_usage_hours,total_pieces,total_pallets,total_cases,total_lines,total_accuracy,tenant_id) values (:dcId, :employeeId, :type, :shift, :jobId, :date, :assignTime, :endTime, :totalTime,:equipment, :equipmentName, :equipmentUsageHours, :totalPieces, :totalPallets,:totalCases, :totalLines, :totalAccuracy, :tenantId)");
        itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<WorkOrders>());
        return itemWriter;
    }

    @Bean
    public JobCompleteNotificationListener jobCompleteNotificationListener(){
        return new JobCompleteNotificationListener();
    }

}