package com.axidio.retinakpidatainputbatch.workorders.controller;

import java.util.Date;

import com.axidio.retinakpidatainputbatch.workorders.dto.KpiDataInputDTO;
import com.axidio.retinakpidatainputbatch.workorders.utils.Constants;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
url: http://localhost:8081/runbatch/workordersjob
 */

@RestController
@RequestMapping("/runbatch")
public class JobController {

    
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job kpiDataInputJob;

    @PostMapping("/workordersjob")
    public String runKpiDataInputJob(@RequestBody KpiDataInputDTO kpiDataInputDTO) {
                
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString(Constants.FILE_NAME_CONTEXT_KEY, kpiDataInputDTO.getFileName()+".csv");
        jobParametersBuilder.addDate("date", new Date(), true);
        Constants.FILE_NAME = kpiDataInputDTO.getFileName();
        Constants.CLIENT_ID = kpiDataInputDTO.getClientId();
        Constants.FEATURE_ID = kpiDataInputDTO.getFeatureId();
        
        try {
            jobLauncher.run(kpiDataInputJob, jobParametersBuilder.toJobParameters());
        } catch (Exception e) {
            e.printStackTrace();
        } 

        return String.format("Work Orders Job submitted successfully.");
    }   

}