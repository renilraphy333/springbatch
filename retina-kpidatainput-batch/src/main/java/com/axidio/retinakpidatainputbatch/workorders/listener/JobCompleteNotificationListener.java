package com.axidio.retinakpidatainputbatch.workorders.listener;

import com.axidio.retinakpidatainputbatch.workorders.utils.Constants;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

public class JobCompleteNotificationListener extends JobExecutionListenerSupport {

	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			System.out.println("JOB COMPLETED!!");
		} else if (jobExecution.getStatus() == BatchStatus.FAILED) {
			System.out.println("JOB FAILED!!");
            System.out.println("Check whether the "+ Constants.FILE_NAME + " table exist in DB....!!!");
		}
	}
    
}
