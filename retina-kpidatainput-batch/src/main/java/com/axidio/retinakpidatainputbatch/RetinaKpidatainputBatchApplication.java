package com.axidio.retinakpidatainputbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetinaKpidatainputBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetinaKpidatainputBatchApplication.class, args);
	}

}
