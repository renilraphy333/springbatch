from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.operators.docker_operator import DockerOperator

default_args = {
        'owner'                 : 'airflow',
        'description'           : 'Running Retina Batch Jobs',
        'depend_on_past'        : False,
        'start_date'            : datetime(2021, 5, 15),
        'email_on_failure'      : False,
        'email_on_retry'        : False,
        'retries'               : 1,
        'retry_delay'           : timedelta(minutes=2)
}

with DAG('retinakpicalculationbatchdag', default_args=default_args, schedule_interval="* * * * *", catchup=False) as dag:
        t1 = DockerOperator(
                task_id='First_Task_Run_KpiCalculation_Batch_Job',
                image='retina-kpicalculation-batch:0.0.1-SNAPSHOT',
                api_version='auto',
                auto_remove=True,
                command="/bin/sleep 30",
                docker_url="unix://var/run/docker.sock",
                network_mode="bridge"
        )


        t1
